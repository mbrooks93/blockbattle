// Copyright 2015 theaigames.com (developers@theaigames.com)

//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at

//        http://www.apache.org/licenses/LICENSE-2.0

//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//	
//    For the full copyright and license information, please view the LICENSE
//    file that was distributed with this source code.

package bot;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import field.ShapeType;
import moves.MoveType;

/**
 * BotStarter class
 * 
 * This class is where the main logic should be. Implement getMoves() to
 * return something better than random moves.
 * 
 * @author Jim van Eeden <jim@starapple.nl>
 */

public class BotStarter {

	int count = 0;
	int countJ = 0;
	int countL = 0;
	

	public BotStarter() {}

	/**
	 * Returns a random amount of random moves
	 * @param state : current state of the bot
	 * @param timeout : time to respond
	 * @return : a list of moves to execute
	 */
	public ArrayList<MoveType> getMoves(BotState state, long timeout) {
		ArrayList<MoveType> moves = new ArrayList<MoveType>();
		Random rnd = new Random();
		
		if(count%2 == 0){

			if(state.currentShape == ShapeType.O){
				for(int i = 0; i < 4; i++){
					moves.add(MoveType.LEFT);
				}
			}
			else if(state.currentShape == ShapeType.Z){
				moves.add(MoveType.TURNLEFT);
				for(int i = 0; i < 1; i++){
					moves.add(MoveType.LEFT);
				}
			}
			else if(state.currentShape == ShapeType.S){
				moves.add(MoveType.TURNLEFT);
				for(int i = 0; i < 1; i++){
					moves.add(MoveType.LEFT);
				}
			}
			else if(state.currentShape == ShapeType.J){
				if(countJ%2 == 0){
					moves.add(MoveType.RIGHT);
					moves.add(MoveType.TURNLEFT);
				}else{
					//moves.add(MoveType.LEFT);
					moves.add(MoveType.TURNRIGHT);
				}
				countJ++;
				count--;
			}
			else if(state.currentShape == ShapeType.L){
				if(countL%2 == 1){
					moves.add(MoveType.RIGHT);
					moves.add(MoveType.TURNLEFT);
				}else{
					//moves.add(MoveType.LEFT);
					moves.add(MoveType.TURNRIGHT);
				}
				countL++;
				count--;
			}
			else if(state.currentShape == ShapeType.T){
				moves.add(MoveType.TURNLEFT);
				for(int i = 0; i < 4; i++){
					moves.add(MoveType.LEFT);
				}
			}
			else{
				for(int i = 0; i < 3; i++){
					moves.add(MoveType.LEFT);
				}
			}
		}else{

			if(state.currentShape == ShapeType.O){
				for(int i = 0; i < 4; i++){
					moves.add(MoveType.RIGHT);
				}
			}
			else if(state.currentShape == ShapeType.Z){
				moves.add(MoveType.TURNLEFT);
				for(int i = 0; i < 3; i++){
					moves.add(MoveType.RIGHT);
				}
			}
			else if(state.currentShape == ShapeType.S){
				moves.add(MoveType.TURNLEFT);
				for(int i = 0; i < 3; i++){
					moves.add(MoveType.RIGHT);
				}
			}
			else if(state.currentShape == ShapeType.J){
				if(countJ%2 == 0){
					moves.add(MoveType.RIGHT);
					moves.add(MoveType.TURNLEFT);
				}else{
					//moves.add(MoveType.LEFT);
					moves.add(MoveType.TURNRIGHT);
				}
				countJ++;
				count--;
			}
			else if(state.currentShape == ShapeType.L){
				if(countL%2 == 1){
					moves.add(MoveType.RIGHT);
					moves.add(MoveType.TURNLEFT);
				}else{
					//moves.add(MoveType.LEFT);
					moves.add(MoveType.TURNRIGHT);
				}
				countL++;
				count--;
			}
			else if(state.currentShape == ShapeType.T){
				moves.add(MoveType.TURNLEFT);
				for(int i = 0; i < 5; i++){
					moves.add(MoveType.RIGHT);
				}
			}
			else{
				for(int i = 0; i < 3; i++){
					moves.add(MoveType.RIGHT);
				}
			}
		}

		count++;

		//		int nrOfMoves = rnd.nextInt(41);
		//		List<MoveType> allMoves = Collections.unmodifiableList(Arrays.asList(MoveType.values()));
		//		for(int n=0; n<=nrOfMoves; n++) {
		//			moves.add(allMoves.get(rnd.nextInt(allMoves.size())));
		//		}

		return moves;
	}


	public static void main(String[] args)
	{
		BotParser parser = new BotParser(new BotStarter());
		parser.run();
	}
}
